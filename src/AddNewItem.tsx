import React, { useState } from "react";
import { NewItemForm } from "./NewInputForm";
import { AddItemButton } from "./styles";

interface AddNewItemProps {
  onAdd(text: string): void;
  toggleButtonText: string;
  dark?: boolean;
}

export const AddNewItem = (props: AddNewItemProps) => {
  const [showForm, setShowForm] = useState(false);
  const { onAdd, toggleButtonText } = props;

  return (
    <>
      <AddItemButton dark onClick={() => setShowForm(true)}>
        {toggleButtonText}
      </AddItemButton>
      {showForm && (
        <NewItemForm
          onAdd={(text) => {
            onAdd(text);
            setShowForm(false);
          }}
        />
      )}
    </>
  );
};
